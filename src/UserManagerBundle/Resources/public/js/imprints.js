
function addImprint() {

    var form_data = new FormData();
    form_data.append("locale", $("#locale").val());
    form_data.append("text", $("#text").val());
    $.ajax({
        url: "/add-imprint",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success:
            function(result)
            {
                new PNotify({
                    title: 'Added',
                    text: result,
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text:   result.responseJSON,
                    type: 'error'
                });
            }
    });
}

function removeProfile(id) {

    $.ajax({
        method: "delete",
        url: "/remove",
        datatype: "json",
        data: { id: id},
        success: function(r) {
            new PNotify({
                title: 'Delete',
                text: r,
                type: 'success'
            });
            $("#"+id).addClass("hide");
        },
        error: function() {
            alert("error");
        }
    });

}

function editProfile() {

    var file_data =$('#profile_picture');
    var form_data = new FormData();
    form_data.append("profile_picture", file_data[0].files[0]) ;
    form_data.append("first_name", $("#first_name").val());
    form_data.append("last_name", $("#last_name").val());
    form_data.append("street", $("#street").val());
    form_data.append("zip", $("#zip").val());
    form_data.append("city", $("#city").val());
    form_data.append("country", $("#country").val());
    form_data.append("phone", $("#phone_number").val());
    form_data.append("birthday", $("#birthday").val());
    form_data.append("email", $("#email").val());
    form_data.append("id", $("#id").val());
    $.ajax({
        url: "/edit",
        type: "POST",
        processData: false, // important
        contentType: false, // important
        data: form_data ,
        success:
            function(result)
            {
                new PNotify({
                    title: 'Updated',
                    text:   result,
                    type: 'success'
                });
            },
        error:
            function (result)
            {
                new PNotify({
                    title: 'Validation Failure',
                    text:   result.responseJSON,
                    type: 'error'
                });


            }
    });
}
