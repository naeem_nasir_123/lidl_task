<?php
/**
 * Created by PhpStorm.
 * User: naeem
 * Date: 24.05.19
 * Time: 09:26
 */

namespace UserManagerBundle\BusinessLogic;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Psr\Container\ContainerInterface;

class UserService
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    /**
     * @return string
     */
    public function listUsers(){
        try{
            $client = new Client();
            $users = $client->request('GET', $this->container->getParameter('user_manager_api_url'), [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json'
                ]])->getBody()->getContents();

            return json_decode($users, true);

        } catch (\Exception $ex) {

            return $ex->getMessage();
        } catch (\GuzzleHttp\Exception\GuzzleException $ex){

            return $ex->getMessage();
        }
    }
}